function tinhTienDien() {
  var hovaten = document.getElementById("txt-ho-ten").value;
  var sokw = document.getElementById("txt-so-kw").value * 1;
  var sokwdau = 50;
  var dongiasokwdau = 500;
  var dongia50kwke = 650;
  var dongia100kwke = 850;
  var dongia150kwke = 1100;
  var dongia350kwtrodi = 1300;

  var result = 0;

  console.log({ hovaten, sokw });

  function tinhTienDien50kwdau() {
    var TienDien50kwdau = sokw * dongiasokwdau;
    result = TienDien50kwdau;
    return result;
  }

  function tinhTienDien50kwke() {
    var TienDien50kwke = sokwdau * dongiasokwdau + (sokw - 50) * dongia50kwke;
    result = TienDien50kwke;
    return result;
  }

  function tinhTienDien100kwke() {
    var TienDien100kwke =
      sokwdau * dongiasokwdau +
      50 * dongia50kwke +
      (sokw - 100) * dongia100kwke;
    result = TienDien100kwke;
    return result;
  }

  function tinhTienDien150kwke() {
    var TienDien150kwke =
      sokwdau * dongiasokwdau +
      50 * dongia50kwke +
      100 * dongia100kwke +
      (sokw - 200) * dongia150kwke;
    result = TienDien150kwke;
    return result;
  }

  function tinhTienDien350kwtrodi() {
    var TienDien350kwtrodi =
      sokwdau * dongiasokwdau +
      50 * dongia50kwke +
      100 * dongia100kwke +
      150 * dongia150kwke +
      (sokw - 350) * dongia350kwtrodi;
    result = TienDien350kwtrodi;
    return result;
  }

  if (sokw <= sokwdau) {
    tinhTienDien50kwdau();
  } else if (sokw <= 100) {
    tinhTienDien50kwke();
  } else if (sokw <= 200) {
    tinhTienDien100kwke();
  } else if (sokw <= 350) {
    tinhTienDien150kwke();
  } else {
    tinhTienDien350kwtrodi();
  }

  document.getElementById("result").innerHTML = ` Họ và tên: ${hovaten} <br>
Tiền điện: ${result}
`;
}
