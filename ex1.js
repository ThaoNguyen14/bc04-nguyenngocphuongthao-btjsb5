function tinhKetQua() {
  var diem1 = document.getElementById("txt-diem-1").value * 1;
  var diem2 = document.getElementById("txt-diem-2").value * 1;
  var diem3 = document.getElementById("txt-diem-3").value * 1;
  var diemchuan = document.getElementById("txt-diem-chuan").value * 1;
  var diemkhuvuc = document.getElementById("khu-vuc").value * 1;
  var diemdoituong = document.getElementById("doi-tuong").value * 1;

  console.log({ diem1, diem2, diem3, diemchuan, diemkhuvuc, diemdoituong });

  if (diem1 <= 0 || diem2 <= 0 || diem3 <= 0) {
    result = `Bạn đã rớt. Do có điểm bằng 0`;
    document.getElementById("result").innerText = result;

    return;
  }

  function tinhDiemTong(diem1, diem2, diem3, diemkhuvuc, diemdoituong) {
    var diemtong = diem1 + diem2 + diem3 + diemkhuvuc + diemdoituong;
    if (diemtong >= diemchuan) {
      result = `Bạn đã đậu. Tổng điểm: ${diemtong}`;
    } else {
      result = ` Bạn đã rớt. Tổng điểm: ${diemtong}`;
    }
    return result;
  }

  tinhDiemTong(diem1, diem2, diem3, diemkhuvuc, diemdoituong);

  document.getElementById("result").innerText = result;
}
